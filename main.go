package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type handler struct{}

func (h *handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("Sup!\n"))
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s port\n", os.Args[0])
		return
	}

	port := ":" + os.Args[1]

	caCert, err := ioutil.ReadFile("client.crt")
	if err != nil {
		panic(err)
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	cfg := &tls.Config{
		ClientAuth: tls.RequireAndVerifyClientCert,
		ClientCAs:  caCertPool,
	}

	srv := &http.Server{
		Addr:      port,
		Handler:   &handler{},
		TLSConfig: cfg,
	}

	fmt.Println("Server started at port", port)
	err = srv.ListenAndServeTLS("server.crt", "server.key")
	if err != nil {
		fmt.Println(err.Error())
	}
}
